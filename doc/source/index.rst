==========================
**datafold** documentation
==========================

Contents
========

.. NOTE: if changing "tutorial_index" in the toctree, the filename also has to be changed
   in include_tutorials.py

.. toctree::
   :maxdepth: 1

   intro
.. toctree::
   :maxdepth: 2

   api
   devapi
   tutorial_index
   references

.. toctree::
   :hidden:

   conda_install_info

-----

.. include:: ../../README.rst

-----

Affiliations
============

Contributors
------------

* **Daniel Lehmberg** (1,2) DL is supported by the German Research Foundation (DFG),
  grant no. KO 5257/3-1 and thanks the research office (FORWIN) of Munich University of
  Applied Sciences and CeDoSIA of TUM Graduate School at the Technical
  University of Munich for their support.
  
* **Felix Dietrich** (2). FD thanks the Technical University of Munich for their support.


1. Munich University of Applied Sciences
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Faculty of Computer Science and Mathematics in
research group "Pedestrian Dynamics" `www.vadere.org <http://www.vadere.org/>`_,

.. image:: _static/img/hm_logo.png
   :height: 110px
   :width: 250px
   :target: https://www.hm.edu/en/index.en.html

2. Technical University of Munich
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Chair of Scientific Computing in Computer Science

.. image:: _static/img/tum_logo.png
   :height: 100px
   :width: 200px
   :target: https://www.tum.de/en/

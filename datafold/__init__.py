#!/usr/bin/env python3

import datafold.appfold
import datafold.dynfold
import datafold.pcfold
import datafold.version

# __version__ is attribute by convention
__version__ = datafold.version.Version.v_short
